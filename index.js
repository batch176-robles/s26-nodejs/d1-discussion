/*
	Client-Server Architecture

	What is a client?
	
	A client is an application which creates requests for resources from a server. A client will trigger an action, in a web development context, through a URL and wait for the response of the server.

	What is a server?

	A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients.

	What is NodeJS?

	NodeJs is a runtime environment which allows us to create/develop backend applications with Javascript. In fact, with Node, we can run JS even without the use of an HTML file.

	Javascript was originally conceptualized and intended to add logic or programmatic flow to Front-End applications. This is the reason why our vanilla Javascript had to be linked to an HTML file before you can run the JS.

	Why is NodeJS so popular?

	Performance -  NodeJS is one of the most performing environment for creating backend applications for JS.

	Familiarity - It uses JS as its language and therefore very familiar for most developers.

	NPM - Node Package Manager - Is the largest registry for node packages. Packages are bits of programs, methods, functions, codes that greatly help in the development of an application.

*/
//console.log("Hello World!");

/*
	We are now able to run a simple nodejs server. Wherein when we added our URL to the browser, the browser, being a client, was able to connect and request data from our server. Our server was then able to send data to our browser.

	We used require() method to be able to use Node.js modules.

	"http" - is a default module from JS.
	
	What is a module?

	A module is a group of codes, methods and functions which is able to help or add functionality into our program. Modules when imported or used via the require() method acts like an object in a JS program.


	The http module let nodejs transfer data or let our client to exchange requests and response with a server via the Hypertext Transfer Protocol

	protocol => http://localhost:4000 <= server/application
	
	Through this URL, our client was able illicit a response from a server.

	This is how clients and server interact/communicate with each other. A client triggers an action from a server via a URL and the server responds accordingly.

		Notes:

		Messages from a client which triggers an action from a server is called a request.

		Messages from a server which is turn used to respond to request is called a response.
*/

let http = require("http");

// console.log(http);

/*
	http.createServer() method allowed to us to create server which is able to handle the requests of a client and respond accordingly.

	.createServer() has an anonymous function as an argument which actually handles our clients request and our server's response. It is able to receive 2 objects, the request and the response. The anonymous function in our createServer() always receives the request object first before the response.

	response.writeHead() - is a method of the response object. This allows us to add headers, which are additional information about our response. We have two argument in our writeHead() method, first is the HTTP status code, an HTTP status code is a code to let the client know about the status of their request, 200 means OK, 404 means the resource cannot be found. 'Content-Type' is one of the more recognizable headers. It is pertaining about the data type of our response.

	response.end() - is a method from the response object which ends the server's response and sends a message/data as a string.

	.listen() - allows us to assign a port to a server. There are several tasks and processes in our computer which are also designated with ports.

	port is a virtual point where connections start and end.
	http://localhost:4000 - localhost - means local machine or computer and 4000 means the port that the server/process is running on. Popularly, for testing local servers, it is run on 4000, but others are available like 8000 or 5000.
*/

http.createServer(function (request, response) {

	/* 
		Our client use a URL to access or trigger an action and response from the server. A client can trigger different actions and responses. How a client triggers different actions and responses is through the use of URL Endpoints.

		http://localhost:4000/ < -- Default Endpoint

		To be able to tell/identify the endpoint of the client's request URL, we access the .url property of our request object.

	*/

	console.log(request.url);

	// How we respond to different requests is by handling client request by endpoints. This is called routing.
	// We'll create an if-else else-if statement to divert or respond differently to our client's request per endpoint.

	if (request.url === "/") {
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end('Hello from our server!');
	} else if (request.url === "/login") {
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end('Hello! Welcome to the Login Page!!');
	} else if (request.url === "/hello") {
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end('Hello! My name is John!');
	} else {
		response.writeHead(404, { 'Content-Type': 'text/plain' })
		response.end('Resource cannot be found.');

	}

	// different response per endpoint is called a route

}).listen(4000);

console.log("Server is running on localhost:4000");

